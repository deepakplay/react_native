import React,{useState} from "react";
import { View, Text } from "react-native";

const App = () => {
  const [width, setWidth] = useState(200);
  const [height, setheight] = useState(60);
  return (
  <>
    <View style={{ backgroundColor: '#444', flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text style={{ color: '#FFF', fontWeight: 'bold', fontSize: 50 }}>Hello, world</Text>
    </View>
    <View style={{ backgroundColor: '#009624', flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text style={{ color: '#FFF', fontWeight: 'bold', fontSize: 30 }}>By Deepak Play</Text>
    </View>
    <View style={{ position: 'absolute', backgroundColor: '#c4001d', borderRadius: 10, top: '50%', left: '50%', right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center', width: width, height: height, transform: [{ translateX: -width/2 }, { translateY: -height/2 }] }}>
      <Text style={{ color: '#FFF', fontWeight: 'bold', fontSize: 30 }}>PlaySec</Text>
    </View>
  </>
)};
export default App;